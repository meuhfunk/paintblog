<?php

namespace App\Controller;

use App\Service\MeteoApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MeteoController extends AbstractController
{
    #[Route('/meteo', name: 'meteo')]
    public function index(
        MeteoApiService $meteoApiService
    ): Response {
        return $this->render(
            'meteo/index.html.twig',
            [
                'controller_name' => 'MeteoController',
                'meteoBordeaux' => $meteoApiService->getBordeauxMeteo(),
            ]
        );
    }
}
