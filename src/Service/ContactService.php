<?php

declare(strict_types=1);


namespace App\Service;


use App\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

/**
 * Class ContactService
 * @package App\Service
 */
class ContactService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistContact(Contact $contact)
    {
        $contact->setIsSend(false)
            ->setCreatedAt(new \DateTime('now'));

        $this->manager->persist($contact);
        $this->manager->flush();
        $this->flash->add('success', 'votre message est bien envoyé');
    }

}
