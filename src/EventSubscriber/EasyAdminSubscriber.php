<?php

namespace App\EventSubscriber;

use App\Entity\Peinture;
use DateTime;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityDeletedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 *
 */
class EasyAdminSubscriber implements EventSubscriberInterface
{
    private $slugger;
    private $security;

    /**
     * @param SluggerInterface $slugger
     */
    public function __construct(SluggerInterface $slugger, Security $security, private SessionInterface $session)
    {
        $this->slugger = $slugger;
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setBlogPostSlugAndDateAndUser'],
            AfterEntityPersistedEvent::class => ['flashMessageAfterPersist'],
            AfterEntityUpdatedEvent::class => ['flashMessageAfterUpdate'],
            AfterEntityDeletedEvent::class => ['flashMessageAfterDelete'],
        ];
    }

    /**
     * @param BeforeEntityPersistedEvent $event
     */
    public function setBlogPostSlugAndDateAndUser(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        $now = new DateTime('now');
        $entity->setCreateAt($now);

        $user = $this->security->getUser();
        $entity->setUser($user);
    }

    /**
     * @param AfterEntityPersistedEvent $event
     */
    public function flashMessageAfterPersist(AfterEntityPersistedEvent $event)
    {
        $instance = $event->getEntityInstance();
        if ($instance instanceof Peinture) {
            $this->session->getFlashBag()->add('success', 'la peinture a été crée correctement');
        } else {
            $this->session->getFlashBag()->add('success', 'le blogpost a été crée correctement');
        }
    }

    /**
     * @param AfterEntityUpdatedEvent $event
     */
    public function flashMessageAfterUpdate(AfterEntityUpdatedEvent $event)
    {
        $instance = $event->getEntityInstance();
        if ($instance instanceof Peinture) {
            $this->session->getFlashBag()->add('success', 'la peinture a été mise a jour avec correctement');
        } else {
            $this->session->getFlashBag()->add('success', 'le blogpost a été mis a jour correctement');
        }
    }

    /**
     * @param AfterEntityDeletedEvent $event
     */
    public function flashMessageAfterDelete(AfterEntityDeletedEvent $event)
    {
        $instance = $event->getEntityInstance();
        if ($instance instanceof Peinture) {
            $this->session->getFlashBag()->add('success', 'la peinture a été supprimée correctement');
        } else {
            $this->session->getFlashBag()->add('success', 'le blogpost a été supprimé correctement');
        }
    }
}
