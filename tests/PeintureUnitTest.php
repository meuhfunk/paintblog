<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\Peinture;
use App\Entity\User;
use PHPUnit\Framework\TestCase;
use DateTime;

/**
 * Class PeintureUnitTest
 * @package App\Tests
 */
class PeintureUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $peinture = new Peinture();
        $datetime = new DateTime();
        $user = new User();
        $categorie = new Categorie();

        $peinture->setNom('nom')
            ->setLargeur('22.20')
            ->setHauteur('22.20')
            ->setEnVente(true)
            ->setPrix('50.00')
            ->setDateRealisation($datetime)
            ->setCreateAt($datetime)
            ->setUser($user)
            ->addCategorie($categorie);


        $this->assertTrue($peinture->getNom() === 'nom');
        $this->assertTrue($peinture->getLargeur() === '22.20');
        $this->assertTrue($peinture->getHauteur() === '22.20');
        $this->assertTrue($peinture->getEnVente() === true);
        $this->assertTrue($peinture->getPrix() === '50.00');
        $this->assertTrue($peinture->getDateRealisation() === $datetime);
        $this->assertTrue($peinture->getCreateAt() === $datetime);
        $this->assertTrue($peinture->getUser() === $user);
    }

    public function testIsFalse(): void
    {
        $peinture = new Peinture();
        $datetime = new DateTime();
        $user = new User();
        $categorie = new Categorie();

        $peinture->setNom('nom')
            ->setLargeur('22.20')
            ->setHauteur('22.20')
            ->setEnVente(true)
            ->setPrix('50.00')
            ->setDateRealisation($datetime)
            ->setCreateAt($datetime)
            ->setUser($user)
            ->addCategorie($categorie);


        $this->assertFalse($peinture->getNom() === 'false');
        $this->assertFalse($peinture->getLargeur() === '22.21');
        $this->assertFalse($peinture->getHauteur() === '22.21');
        $this->assertFalse($peinture->getEnVente() === false);
        $this->assertFalse($peinture->getPrix() === '51.00');
    }

    public function testIsEmpty(): void
    {
        $peinture = new Peinture();

        $this->assertEmpty($peinture->getNom());
        $this->assertEmpty($peinture->getLargeur());
        $this->assertEmpty($peinture->getHauteur());
        $this->assertEmpty($peinture->getEnVente());
        $this->assertEmpty($peinture->getPrix());
    }
}

