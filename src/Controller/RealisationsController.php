<?php

namespace App\Controller;

use App\Entity\Peinture;
use App\Repository\PeintureRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RealisationsController
 * @package App\Controller
 */
class RealisationsController extends AbstractController
{
    #[Route('/realisations', name: 'realisations')]
    public function index(
        PeintureRepository $peintureRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $peintureRepository->findAll();
        $peintures = $paginator->paginate($data, $request->query->getInt('page', 1), 6);
        return $this->render(
            'realisations/realisations.html.twig',
            [
                'peintures' => $peintures,
            ]
        );
    }

    /**
     * @Route("/peinture/{slug}", name="peinture_detail")
     *
     * @param Peinture $peinture
     * @param Request $request
     * @return Response
     */
    public function detail(
        Peinture $peinture,
        Request $request,
    ): Response {

        return $this->render(
            'realisation/detailRealisation.html.twig',
            [
                'peinture' => $peinture,

            ]
        );
    }
}
