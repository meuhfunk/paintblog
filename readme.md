# PaintBlog

Site internet de peintures

## Environnement de développement

### Pré-requis

* PHP 8
* Composer
* Symfony CLI
* Docker
* Docker-compose
* nodejs et npm

### Verification des Pré-requis

symfony check:requirements

### Lancer l'environnement de développement

* composer install
* npm install
* npm run build
* docker-compose up -d
* symfony serve -d
