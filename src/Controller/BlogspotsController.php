<?php

namespace App\Controller;

use App\Entity\Blogpost;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Repository\BlogpostRepository;
use App\Repository\CommentaireRepository;
use App\Service\CommentaireService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogspotsController extends AbstractController
{
    #[Route('/blogspots', name: 'blogspots')]
    public function index(
        BlogpostRepository $blogpostRepository,
        PaginatorInterface $paginator,
        Request $request

    ): Response {
        $data = $blogpostRepository->findAll();
        $blogsposts = $paginator->paginate($data, $request->query->getInt('page', 1), 6);



        return $this->render(
            'blogspots/blogspots.html.twig',
            [
                'blogspots' => $blogsposts,
            ]
        );
    }
    /**
     * @Route("/actualites/{slug}", name="actualites_detail")
     *
     * @param Blogpost $blogpost
     * @param Request $request
     * @param CommentaireService $commentaireService
     * @return Response
     */
    public function detail(
        Blogpost $blogpost,
        Request $request,
        CommentaireService $commentaireService
    ): Response {

        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);


        return $this->render(
            'blogpost/detail.html.twig',
            [
                'blogpost' => $blogpost,

            ]
        );
    }
}
