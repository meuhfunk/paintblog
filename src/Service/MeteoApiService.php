<?php


namespace App\Service;


use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class MeteoApiService
 * @package App\Service
 */
class MeteoApiService
{
    private $client;

    /**
     * MeteoApiService constructor.
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getBordeauxMeteo(): array
    {
        $response = $this->client->request(
            'GET',
            'https://www.metaweather.com/api/location/580778/'
        );

        $statusCode = $response->getStatusCode();
        $contentType = $response->getHeaders()['content-type']['0'];
        $content = $response->getContent();
        $content = $response->toArray();

        return $content;
    }
}
